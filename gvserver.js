var express = require('express'),
    users = require('./api/users');
    files = require('./api/files');
    groups = require('./api/groups');
    parofiles = require('./api/parofiles');

var app = express();

app.configure(function() {

   var cwd = __dirname;
   app.use(express.logger('dev'));
   app.use(express.bodyParser({uploadDir:cwd+'/uploads'}));
});

app.get('/main', function(req, res, next) {

html = '<form method="post" enctype="multipart/form-data" action="/file-upload"> \
    <input type="file" name="thumbnail"> \
    <input type="submit"> \
</form>';


res.send(html);

});

// Users management
// app.post('/user', users.register);
//  app.post('/user/login', users.login);
// app.delete('/user/:userid', users.delete);
app.get('/lastaccess', users.lastaccess);
app.get('/user/:userid', users.info);
app.post('/update_token', users.update_token);
app.get('/pushtest/:userid', users.pushtest);

// ParoParo token management
app.post('/paro_token', users.paro_token);  // update user token
app.get('/paro_lastaccess', users.paro_lastaccess);   // see last access 
app.post('/paro_push', users.paro_push);  // update user token

// VideoGroup APIs
app.get('/group/view/:groupid', groups.view);
app.post('/group/create', groups.create);
app.get('/group/list/:userid', groups.list);
app.delete('/group/delete/:groupid/:userid', groups.delete);

// ParoParo file upload
app.get ('/paro/video_list', parofiles.video_list);
app.post('/paro/upload/:userid', parofiles.upload);
app.get('/paro/:videoid', parofiles.download);
app.get('/paro/thumbnail/:videoid', parofiles.tdownload);

// File upload
app.get ('/final_video', files.final_video);
app.get ('/video/cleanup', files.cleanup);
app.get ('/video/emptytrash', files.emptytrash);
app.post('/video/upload/:groupid/:userid', files.upload);
app.get ('/video/thumbnail/:videoid', files.tdownload);
app.post('/video/thumb_upload/:videoid', files.tupload);
app.post('/video/fupload/:groupid', files.finalupload);
app.get ('/video/:videoid', files.download);
// Thumbnail

console.log(process.versions);

var port = 8080;
app.listen(port);
console.log('Server running at port: ' + port);

