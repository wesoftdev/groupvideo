var fs = require('fs');
var uuid = require('./uuid');
var dbase = require('./dbase');
var apns_lib = require('./apns_lib');
var thumbler = require('video-thumb');

function app_path() {

   var cwd = __dirname;
   var pos = cwd.lastIndexOf("/");
   var path = cwd.substring(0,pos);
   return path;
   
}

// ParoParo file upload
// app.get ('/paro/video_list', parofiles.video_list);
// app.post('/paro/upload/:userid', parofiles.upload);


exports.video_list = function(req, res) {
   db.collection('pvideo', function(err, collection) {
      collection.find({time:{$exists:true}}, {_id:0}, {sort: {"time":-1}, limit:1000}).toArray(function(err, items) {
         if (err) {
            res.send({'ok': false, 'error': "Database error"});
         } else {
            if(!items) 
               res.send({'ok': false, 'error':'Video does not exist'});
            else
               res.send(items);
         }
      });
   });
//   res.send({'ok':true});
}


exports.upload = function(req, res) {
   var userid = req.params.userid;
   
   var fileid = uuid.uuid(12);

   var tmp_path = req.files.video.path;
   var filename = req.files.video.name;
   var pos = filename.lastIndexOf(".");

   var ext = ""
   if(pos>=0)
      ext = filename.substring(pos, filename.length)
   var target_path = app_path() + '/pvideo/' + fileid + ext;  
   
   if(ext != '.mp4') {
      fs.unlink(tmp_path, function(err) {
         if (err) throw err;
         res.send({"ok": false, "error": "File type is not mp4"});
      });      
   }
   
   fs.rename(tmp_path, target_path, function(err) {
      if (err) {
         res.send({"ok":false, "error": "Server unable to store the file"});
         return;
      }

      // Create thumbnail
      
      console.log('Src: ' + target_path);
      console.log('Dst: ' + app_path() + '/pvideo/' + fileid + '.png');

      thumbler.extract(target_path, app_path() + '/pvideo/' + fileid +'.png', '00:00:01', '200x106', function() {
         console.log('snapshot saved: ' + fileid + '.png');
      });      
      // Add info to database
      db.collection('pvideo', function(err, collection) {
         if(err) {
            res.send({'ok': false, 'error': 'Database error'});
            return;
         }
         
         collection.insert({userid: userid, time:new Date(), videoid:fileid}, function(err, item) {
            if(err) {
               res.send({"ok":false, "error": err});
            }
         });
      });
      
      res.send({"ok":true, "videoid": fileid});
   });   
}

exports.tdownload = function(req, res) {

   videoid = req.params.videoid;

   console.log(app_path() + '/pvideo/' + videoid + '.png');
   
   fs.exists(app_path() + '/pvideo/'+videoid+'.png', function (exists) {
      if(exists)
         res.sendfile(app_path() + '/pvideo/'+videoid +'.png');
      else
         res.send({"ok": false, "error":"Video ID does not exist"});
   });
   
}

exports.download = function(req, res) {

   videoid = req.params.videoid;

   console.log(app_path() + '/pvideo/' + videoid + '.mp4');
   
   fs.exists(app_path() + '/pvideo/'+videoid+'.mp4', function (exists) {
      if(exists)
         res.sendfile(app_path() + '/pvideo/'+videoid +'.mp4');
      else
         res.send({"ok": false, "error":"Video ID does not exist"});
   });
   
}