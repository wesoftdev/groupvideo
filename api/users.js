var dbase = require('./dbase');
var apns_lib = require('./apns_lib');

db = dbase.db;

// Users management
// app.post('/user', users.register);
// app.post('/user/login', users.login);
// app.get('/user/:userid', users.info);


// POST /update_token
exports.update_token = function(req, res) {
   var body = req.body;
   
   var userid = body.userid;
   var token = body.token;
   var cert_id = body.cert_id;
   var username = body.username;

   if(username=='(null)' || userid=='(null}') {
      res.send({'ok': false, 'error': 'userid or userid is null'});
      return;
   }
   db.collection('users', function(err, collection) {
      body.last_update = new Date();

      collection.update({'_id':userid}, {$set:{last_update: new Date(), 'token':token, 'cert_id':cert_id, 'username':username, 'ver':body.ver, 'rev':body.rev}},  {'upsert':true}, function(err, result) {
         if (err) {
            res.send({'ok': false, 'error': "Update token failed"});
         } else {
            res.send({'ok': true});
         }
      });
   });
}

// POST /paro_token
exports.paro_token = function(req, res) {
   var body = req.body;
   
   var token = body.token;
   var cert_id = body.cert_id;

   db.collection('paro_users', function(err, collection) {
      body.last_update = new Date();

      collection.update({'token':token}, {$set:{last_update: new Date(), 'cert_id':cert_id, 'ver':body.ver, 'rev':body.rev}},  {'upsert':true}, function(err, result) {
         if (err) {
            res.send({'ok': false, 'error': "Update token failed"});
         } else {
            res.send({'ok': true});
         }
      });
   });
}

// POST /paro_push
exports.paro_push = function(req, res) {
   var body = req.body;
   
   var message = body.message;

   db.collection('paro_users', function(err, collection) {
      collection.find({}).toArray(function(err, items) {
         if (err) {
            res.send({'ok': false, 'error': "Database error"});
         } else {
            if(!items) 
               res.send({'ok': false, 'error':'User does not exist'});
            else {
               for (var i in items) {
                  apns_lib.sendMessageToToken(items[i].token, message, items[i].cert_id);
               }
               res.send({'ok': true});
            }
         }
      });
   });
}

// POST /user
exports.register = function(req, res) {
   var body = req.body;
   
   var data = {_id: body.userid, time: new Date(), password: body.password};
   
   db.collection('users', function(err, collection) {
      collection.insert(data, {safe:true}, function(err, result) {
         if (err) {
            res.send({'ok': false, 'error': "Registration failed"});
         } else {
            res.send({'ok': true});
         }
      });
   });
}

// POST /user/login
exports.login = function(req, res) {
   var body = req.body;
   var userid = body.userid;
   
   db.collection('users', function(err, collection) {
      collection.findOne({'_id':userid}, function(err, item) {
         if (err) {
            res.send({'ok': false, 'error': "Database error"});
         } else {
            if(!item) {
               res.send({'ok': false, 'error':'User not found'});
               return;
            }
            if(body.password == item.password) {
               res.send({'ok': true});

               collection.update({'_id':userid}, {$set:{'token':body.token, 'dev_mode':body.dev_mode}}, function(err, result) {
                  console.log('updated: ' + result);
                  if (err) {
                     res.send({'ok': false, 'error': "Update token failed"});
                  } else {
                     res.send({'ok': true});
                  }
               });
            }
            else
               res.send({'ok': false, 'error':'Password incorrect'});
         }
      });
   });
}

// app.get('/pushtest/:userid', users.pushtest);

// GET /pushtest/:userid
exports.pushtest = function(req, res) {
   var userid = req.params.userid;
   
   var sent = true;
   sent = apns_lib.sendMessage(userid, "[" + formatted_now() +"] Push test.");
   sent = true;
   if(sent)
      res.send({'ok':true});
   else 
      res.send({'ok':false, 'error': 'No such userid or this user has no token'});
}

function formatted_now() {
   var date = new Date();
   date.setHours(date.getHours()+8);
   return date.toISOString().replace(/T/, ' ').replace(/\..+/,'');
}

// GET /user/:userid
exports.info = function(req, res) {
   var userid = req.params.userid;
   
   db.collection('users', function(err, collection) {
      collection.findOne({'_id':userid}, function(err, item) {
         if (err) {
            res.send({'ok': false, 'error': "Database error"});
         } else {
            if(!item) 
               res.send({'ok': false, 'error':'User does not exist'});
            else
               res.send({'ok': true, 'user_info': item});
         }
      });
   });
}

// GET /lastaccess
exports.lastaccess = function(req, res) {
   db.collection('users', function(err, collection) {
      collection.find({}, {sort: {"last_update":-1}, limit:100}).toArray(function(err, items) {
         if (err) {
            res.send({'ok': false, 'error': "Database error"});
         } else {
            if(!items) 
               res.send({'ok': false, 'error':'User does not exist'});
            else
               res.send({'ok': true, 'user_info': items});
         }
      });
   });
//   res.send({'ok':true});
}

// GET /paro_lastaccess
exports.paro_lastaccess = function(req, res) {
   db.collection('paro_users', function(err, collection) {
      collection.find({}, {sort: {"last_update":-1}, limit:100}).toArray(function(err, items) {
         if (err) {
            res.send({'ok': false, 'error': "Database error"});
         } else {
            if(!items) 
               res.send({'ok': false, 'error':'User does not exist'});
            else
               res.send({'ok': true, 'user_info': items});
         }
      });
   });
//   res.send({'ok':true});
}

// DELETE /user/:userid
exports.delete = function(req, res) {
   var userid = req.params.userid;
   
   db.collection('users', function(err, collection) {
      collection.remove({'_id':userid}, {safe:true}, function(err, result) {
         if (err) {
            res.send({'ok': false, 'error': "Database error"});
         } else {
            if(result == 0)
               res.send({'ok':false, 'error':'User not exist'});
            else
               res.send({'ok': true});
         }
      });
   });

}
