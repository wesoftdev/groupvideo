var dbase = require('./dbase');

var fs = require('fs');
var crypto = require('crypto');
var tls = require('tls');
var cwd = __dirname;

var apns_obj = [];
/*
var last_connect = [];
var certPem = [];
var keyPem = [];
var options = [];
var stream = [];
var p_server = [];
var p_name = [];
*/
var caCert = fs.readFileSync(cwd+'/applewwca.cer', encoding='ascii');

// WeSoft Vigo production
apns_obj[0] = {};
apns_obj[0].certPem = fs.readFileSync(cwd+'/vigo-prod-cert.pem', encoding='ascii');
apns_obj[0].keyPem = fs.readFileSync(cwd+'/vigo-prod-key.pem', encoding='ascii');
apns_obj[0].options = { key: apns_obj[0].keyPem, cert: apns_obj[0].certPem, ca: [ caCert ], rejectUnauthorized:false };
apns_obj[0].server = 'gateway.push.apple.com';
apns_obj[0].name = 'WeSoft Vigo Prod';

// WeSoft Vigo development
apns_obj[1] = {};
apns_obj[1].certPem = fs.readFileSync(cwd+'/vigo-dev-cert.pem', encoding='ascii');
apns_obj[1].keyPem = fs.readFileSync(cwd+'/vigo-dev-key.pem', encoding='ascii');
apns_obj[1].options = { key: apns_obj[1].keyPem, cert: apns_obj[1].certPem, ca: [ caCert ], rejectUnauthorized:false };
apns_obj[1].server = 'gateway.sandbox.push.apple.com';
apns_obj[1].name = 'WeSoft Vigo Dev';

// iMusic tech VigoVigo production
apns_obj[2] = {};
apns_obj[2].certPem = fs.readFileSync(cwd+'/ck-cert.pem', encoding='ascii');
apns_obj[2].keyPem = fs.readFileSync(cwd+'/ck-key.pem', encoding='ascii');
apns_obj[2].options = { key: apns_obj[2].keyPem, cert: apns_obj[2].certPem, ca: [ caCert ], rejectUnauthorized:false };
apns_obj[2].server = 'gateway.push.apple.com';
apns_obj[2].name = 'iMusic Vigo Prod';

// iMusic tech ParoParo production
apns_obj[3] = {};
apns_obj[3].certPem = fs.readFileSync(cwd+'/paro-cert.pem', encoding='ascii');
apns_obj[3].keyPem = fs.readFileSync(cwd+'/paro-key.pem', encoding='ascii');
apns_obj[3].options = { key: apns_obj[3].keyPem, cert: apns_obj[3].certPem, ca: [ caCert ], rejectUnauthorized:false };
apns_obj[3].server = 'gateway.push.apple.com';
apns_obj[3].name = 'iMusic Paro Prod';

// WeSoft Vigo Dev
apns_obj[4] = {};
apns_obj[4].certPem = fs.readFileSync(cwd+'/paro-dev-cert.pem', encoding='ascii');
apns_obj[4].keyPem = fs.readFileSync(cwd+'/paro-dev-key.pem', encoding='ascii');
apns_obj[4].options = { key: apns_obj[4].keyPem, cert: apns_obj[4].certPem, ca: [ caCert ], rejectUnauthorized:false };
apns_obj[4].server = 'gateway.sandbox.push.apple.com';
apns_obj[4].name = 'WeSoft Paro Dev';

// WeSoft Vigo development
apns_obj[5] = {};
apns_obj[5].certPem = fs.readFileSync(cwd+'/paro-prod-cert.pem', encoding='ascii');
apns_obj[5].keyPem = fs.readFileSync(cwd+'/paro-prod-key.pem', encoding='ascii');
apns_obj[5].options = { key: apns_obj[5].keyPem, cert: apns_obj[5].certPem, ca: [ caCert ], rejectUnauthorized:false };
apns_obj[5].server = 'gateway.push.apple.com';
apns_obj[5].name = 'WeSoft Paro Prod';

function sendMessage(userid, msg) {
   db.collection('users', function(err, collection) {
      collection.findOne({'_id':userid}, function(err, item) {
         if(err) {
            console.log(err);
            return false;
         }
         if(!item) {
            return false;
         }
         
         var payload = { aps: {alert:msg, sound:"default"}};
         var hextoken = item.token;
         if(!hextoken) {
            console.log('No token');
            return false;
         }
         var buffer = createSimplePayload(hextoken, payload);
         if(!buffer)
            return false;
         
         var app_id = item.cert_id;
         if(!apns_obj[app_id].stream) {
            apns_obj[app_id].stream = connect_stream(app_id);
         }
         apns_obj[app_id].last_connect = new Date();
         apns_obj[app_id].stream.write(buffer);
         return true;
      });
   });
}

function sendMessageToToken(token, msg, cert_id) {
         
   var payload = { aps: {alert:msg, sound:"default"}};
   var hextoken = token;
   var buffer = createSimplePayload(hextoken, payload);
   if(!buffer)
      return false;
   
   if(!apns_obj[cert_id].stream) {
      apns_obj[cert_id].stream = connect_stream(cert_id);
   }
   apns_obj[cert_id].last_connect = new Date();
   apns_obj[cert_id].stream.write(buffer);
   return true;

}


var disconnect = function() {
   var now = new Date();
   for(app_id in apns_obj) {
      if(apns_obj[app_id].stream) {
         if(now - apns_obj[app_id].last_connect > 60000) {
            console.log('disconnecting: ' + apns_obj[app_id].name);
            apns_obj[app_id].stream.end();
            apns_obj[app_id].stream = null;
         }
      }
   }
   
   /*
   if(stream_p) {
      if(now-last_connect_p > 60000) {
         console.log('disconnecting stream_p');
         stream_p.end();
         stream_p = null;
      }
   }
   if(stream_d) {
      if(now-last_connect_d > 60000) {
         console.log('disconnecting stream_d');
         stream_d.end();
         stream_d = null;
      }
   }*/
   
   setTimeout(disconnect, 10000);
}

setTimeout(disconnect, 10000);

function connect_stream(id) {
   var s = tls.connect(2195, apns_obj[id].server, apns_obj[id].options, function() {
      console.log('client [' + apns_obj[id].name + '] connected ' + (s.authorized ? 'authorized' : 'unauthorized'));
      //timerId = setInterval(consume, 1000);
   });

   s.id = id;   
   s.on('end', function() {
      console.log('stream is ended');
      apns_obj[s.id].stream = null;
   });

   s.on('close', function() {
      console.log('stream is closed');
      apns_obj[s.id].stream = null;
   });

   s.on('error', function() {
      console.log('stream has error');
      apns_obj[s.id].stream = null;
   });
   
   if(s) {
      apns_obj[id].last_connect = new Date();
      console.log('TLS connected: ' + apns_obj[id].name);
   } else {
      console.log('TLS not connected: ' + apns_obj[id].name);
   }
   return s;
};  


/*
function connect_stream_d() {
   var s = tls.connect(2195, 'gateway.sandbox.push.apple.com', options_d, function() {
      console.log('client connected ' + (s.authorized ? 'authorized' : 'unauthorized'));
      //timerId = setInterval(consume, 1000);
   });
   
   if(s) {
      last_connect_d = new Date();
      console.log('TLS connected (DEV)');
   } else {
      console.log('TLS not connected (DEV)');
   }
   return s;
};  

function connect_stream_p() {
   var s = tls.connect(2195, 'gateway.push.apple.com', options_p, function() {
      console.log('client connected ' + (s.authorized ? 'authorized' : 'unauthorized'));
      //timerId = setInterval(consume, 1000);
   });
   
   if(s) {
      last_connect_p = new Date();
      console.log('TLS connected (PROD)');
   } else {
      console.log('TLS not connected (PROD)');
   }
   return s;
};  
*/


function createEnhancedPayload(hextoken, pushnd) {

   // var pushnd = { aps: { alert:'This is a test' }};
   //var hextoken = '3c8642bc8d81bb69f5b14ff50acfced20b4630f48be441e4d2a80afb0ea7e180';

   // Construct the message
   payload = JSON.stringify(pushnd);
   var payloadlen = Buffer.byteLength(payload, 'utf-8');
   var tokenlen = 32;
   var buffer = new Buffer(1 + 4 + 4 + 2 + tokenlen + 2 + payloadlen);
   var i = 0;
   buffer[i++] = 1; // command
   var msgid = 0xbeefcace;  // message identifier, can be left 0
   buffer[i++] = msgid >> 24 & 0xff;
   buffer[i++] = msgid >> 16 & 0xff;
   buffer[i++] = msgid >> 8 & 0xff;
   buffer[i++] = msgid & 0xff;
   
   // expiry in epoch seconds (1 hour)
   var seconds = Math.round(new Date().getTime() / 1000) + 1*60*60;
   buffer[i++] = seconds >> 24 & 0xff;
   buffer[i++] = seconds >> 16 & 0xff;
   buffer[i++] = seconds >> 8 & 0xff;
   buffer[i++] = seconds & 0xff;

   buffer[i++] = tokenlen >> 8 & 0xff;  // token length
   buffer[i++] = tokenlen & 0xff;
   var token = new Buffer(hextoken, 'hex');
   token.copy(buffer, i, 0, tokenlen)
   i += tokenlen;

   buffer[i++] = payloadlen >> 8 & 0xff; // payload length
   buffer[i++] = payloadlen & 0xff;

   //console.log('Payload:'+payload);

   var payload = Buffer(payload);
   payload.copy(buffer, i, 0, payloadlen);

   return buffer;
}

function createSimplePayload(hextoken, pushnd) {

   // var pushnd = { aps: { alert:'This is a test' }};
   //var hextoken = '3c8642bc8d81bb69f5b14ff50acfced20b4630f48be441e4d2a80afb0ea7e180';

   if(hextoken.length != 64) {
      console.log('Invalid token length');
      return null; 
   }

   // Construct the message
   payload = JSON.stringify(pushnd);
   var payloadlen = Buffer.byteLength(payload, 'utf-8');
   var tokenlen = 32;
   var buffer = new Buffer(1 + 2 + tokenlen + 2 + payloadlen);
   var i = 0;
   buffer[i++] = 0; // command
   
   buffer[i++] = tokenlen >> 8 & 0xff;  // token length
   buffer[i++] = tokenlen & 0xff;
   var token = new Buffer(hextoken, 'hex');
   token.copy(buffer, i, 0, tokenlen)
   i += tokenlen;

   buffer[i++] = payloadlen >> 8 & 0xff; // payload length
   buffer[i++] = payloadlen & 0xff;

   //console.log('Payload:'+payload);

   var payload = Buffer(payload);
   payload.copy(buffer, i, 0, payloadlen);

   // console.log('Command: ' + buffer.toString('hex'));
   return buffer;
}

/*
function buffer2hex(s) {
   var i, f = 0, a = [];
   //s += '';
   f = s.length;
   for (i=0; i<f; i++) {
   	a[i] = s.readUInt8(i).toString(16).replace(/^([\da-f])$/, "0$1");
   }
   return a.join('');
}

function hex2buffer(hexstr) {
   buf = new Buffer(hexstr.length / 2);
   for (var i=0; i < hexstr.length/2; i++) {
      buf[i] = (parseInt(hexstr[i*2], 16) << 4) + (parseInt(hexstr[i*2+1], 16));
   }
   return buf;
}
*/

module.exports.createSimplePayload = createSimplePayload;
module.exports.createEnhancedPayload = createEnhancedPayload;
module.exports.sendMessage = sendMessage;
module.exports.sendMessageToToken = sendMessageToToken;
// module.exports.buffer2hex = buffer2hex;
// module.exports.hex2buffer = hex2buffer;
