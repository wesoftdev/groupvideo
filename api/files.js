var fs = require('fs');
var uuid = require('./uuid');
var dbase = require('./dbase');
var apns_lib = require('./apns_lib');
var thumbler = require('video-thumb');

function app_path() {

   var cwd = __dirname;
   var pos = cwd.lastIndexOf("/");
   var path = cwd.substring(0,pos);
   return path;
   
}

// app.get('/video/thumbnail/:videoid', files.tdownload);
// app.post('/video/upload/thumbnail/:videoid', files.tupload);


// GET /final_video
exports.final_video = function(req, res) {
   db.collection('groups', function(err, collection) {
      collection.find({fv_time:{$exists:true}}, {members:0,groupid:0,time:0,deleted:0,videoid:0,_id:0}, {sort: {"fv_time":-1}, limit:1000}).toArray(function(err, items) {
         if (err) {
            res.send({'ok': false, 'error': "Database error"});
         } else {
            if(!items) 
               res.send({'ok': false, 'error':'Video does not exist'});
            else
               res.send(items);
         }
      });
   });
//   res.send({'ok':true});
}


exports.tdownload = function(req, res) {

   videoid = req.params.videoid;

   console.log(app_path() + '/video/' + videoid + '.png');
   
   fs.exists(app_path() + '/video/'+videoid+'.png', function (exists) {
      if(exists)
         res.sendfile(app_path() + '/video/'+videoid +'.png');
      else
         res.send({"ok": false, "error":"Video ID does not exist"});
   });
   
}

exports.tupload = function(req, res) {

   var videoid = req.params.videoid;

   var tmp_path = req.files.thumbnail.path;
   var filename = req.files.thumbnail.name;
   var pos = filename.lastIndexOf(".");

   var ext = ""
   if(pos>=0)
      ext = filename.substring(pos, filename.length)
   var target_path = app_path() + '/video/' + videoid + ext;  
   
   if(ext != '.png') {
      fs.unlink(tmp_path, function(err) {
         if (err) throw err;
         res.send({"ok": false, "error": "File type is not png"});
      });      
   }
   
   fs.rename(tmp_path, target_path, function(err) {
      if (err) {
         res.send({"ok":false, "error": "Server unable to store the file"});
         return;
      }
      
      res.send({"ok":true});
   });
}

exports.emptytrash = function(req, res) {

   db.collection('groups', function(err, collection) {
      if(err)
         res.send({'ok':false, 'err': err});
      collection.find({'deleted':true}, {sort: {"time":-1}}).toArray(function(err, items) {
         if(err)
            res.send({'ok':false, 'err': 'database error'});
         
         for (item in items) {
            this_item = items[item];
            var members = this_item.members;
            var all_deleted = true;
            for (x in members) {
               if(!members[x].deleted) {
                  all_deleted = false;
                  break;
               }
            }
            if(all_deleted) {
               console.log('delete: '+this_item._id);
               collection.remove({'_id':this_item._id}, {safe:true}, function(err, result) {
                  console.log('num: ' + result);
               });
            }
         }
      });
   });
   res.send({'ok':true});
}

exports.cleanup = function(req ,res) {

   var target_path = app_path() + '/video/';  
   db.collection('groups', function(err, collection) {
      if(err) {
         res.send({'ok': false, 'error': 'Database error'});
         return;
      }
      collection.find({}, {sort: {"time":-1}}).toArray(function(err, items) {
         var fileid = [];
         if(err) {
            res.send({"ok":false, "error": err});
         }
         for (i in items) {
            if(items[i].videoid)
               fileid.push(items[i].videoid + '.mp4');
            if(items[i].final_videoid)
               fileid.push(items[i].final_videoid + '.mp4');
            for(j in items[i].members) {
               if(items[i].members[j].videoid)
                  fileid.push(items[i].members[j].videoid+'.mp4');
            }    
         }
         
         fs.readdir( target_path, function(err, list) {
           
            for (fid in list) {
               var found = false;
               for (eid in fileid) {
                  if(list[fid] == fileid[eid]) {
                     found = true;
                     break;
                  }
               }
               
               if(!found) {
                  tmp_path = target_path + list[fid];
                  fs.unlink(tmp_path, function(err) {
                     if (err) throw err;
                     res.send({"ok": false, "error": "Unable to delete file", 'path':tmp_path});
                  });
               }      
            }
         });
         
         
         res.send({'ok':true});
      });
   });
}   

exports.upload = function(req, res) {

   var groupid = req.params.groupid;
   var userid = req.params.userid;

   var fileid = uuid.uuid(12);

   var tmp_path = req.files.video.path;
   var filename = req.files.video.name;
   var pos = filename.lastIndexOf(".");

   var ext = ""
   if(pos>=0)
      ext = filename.substring(pos, filename.length)
   var target_path = app_path() + '/video/' + fileid + ext;  
   
   if(ext != '.mp4') {
      fs.unlink(tmp_path, function(err) {
         if (err) throw err;
         res.send({"ok": false, "error": "File type is not mp4"});
      });      
   }
   
   fs.rename(tmp_path, target_path, function(err) {
      if (err) {
         res.send({"ok":false, "error": "Server unable to store the file"});
         return;
      }

      // Create thumbnail
      
      console.log('Src: ' + target_path);
      console.log('Dst: ' + app_path() + '/video/' + fileid + '.png');

      thumbler.extract(target_path, app_path() + '/video/' + fileid +'.png', '00:00:01', '200x106', function() {
         console.log('snapshot saved: ' + fileid + '.png');
      });      
      // Add info to database
      db.collection('groups', function(err, collection) {
         if(err) {
            res.send({'ok': false, 'error': 'Database error'});
            return;
         }
         
         collection.update({'_id':groupid, 'creator':userid}, {$set:{'videoid':fileid}}, function(err, items) {
            if(err) {
               res.send({"ok":false, "error": err});
            }
         });
         
         collection.update({'_id':groupid, 'members.userid':userid}, {$set:{'members.$.videoid':fileid}}, function(err, items) {
            if(err) {
               res.send({"ok":false, "error": err});
            }
            if(items>0) {
               notifyVideoUploaded(groupid, userid);
            }
         });
      });
      
      res.send({"ok":true, "videoid": fileid});
   });
}

exports.finalupload = function(req, res) {

   var groupid = req.params.groupid;

   var fileid = uuid.uuid(12);

   var tmp_path = req.files.video.path;
   var filename = req.files.video.name;
   var pos = filename.lastIndexOf(".");

   var ext = ""
   if(pos>=0)
      ext = filename.substring(pos, filename.length)
   var target_path = app_path() + '/video/' + fileid + ext;  
   
   if(ext != '.mp4') {
      fs.unlink(tmp_path, function(err) {
         if (err) throw err;
         res.send({"ok": false, "error": "File type is not mp4"});
      });      
   }
   
   fs.rename(tmp_path, target_path, function(err) {
      if (err) {
         res.send({"ok":false, "error": "Server unable to store the file"});
         return;
      }

      // Create thumbnail
      thumbler.extract(target_path, app_path() + '/video/' + fileid + '.png', '00:00:01', '200x106', function() {
         console.log('snapshot saved: ' + fileid + '.png');
      });      
      // Add info to database
      
      db.collection('groups', function(err, collection) {
         if(err) {
            res.send({'ok': false, 'error': 'Database error'});
            return;
         }
         collection.update({'_id':groupid}, {$set:{'final_videoid':fileid, 'fv_time':new Date()}}, function(err, items) {
            if(err) {
               res.send({"ok":false, "error": err});
            }
            notifyFinalVideo(groupid);
         });
      });
      
      res.send({"ok":true, "final_videoid": fileid});
   });
}

function notifyVideoUploaded(groupid, userid) {
   getGroupInfo(groupid, function(groupinfo) {
      getUserInfo(userid, function(userinfo) {
         if(groupinfo && userinfo) {
            apns_lib.sendMessage(groupinfo.creator, userinfo.username +' has uploaded a video.');
         }
      });
   });
}

function getGroupInfo(groupid, next) {
   db.collection('groups', function(err, collection) {
      if(err) {
         next(null);
      }
      collection.findOne({'_id':groupid}, function(err, item) {
         if(err) {
            next(null);
         }
         next(item);
      });
   });
}

function getUserInfo(userid, next) {
   db.collection('users', function(err, collection) {
      if(err) {
         next(null);
      }
      collection.findOne({'_id':userid}, function(err, item) {
         if(err) {
            next(null);
         }
         next(item);
      });
   });
}
   

function notifyFinalVideo(groupid) {
   db.collection('groups', function(err, collection) {
      if(err) {
         res.send({'ok': false, 'error': 'Database error'});
         return;
      }
      collection.findOne({'_id':groupid}, function(err, items) {
         if(err) {
            res.send({"ok":false, "error": err});
         }
         the_creator = items.creator_name;
         for(x in items.members) {
            apns_lib.sendMessage(items.members[x].userid, '['+ the_creator +'] has completed a group video.');
         }
      });
   });
}

exports.download = function(req, res) {

   videoid = req.params.videoid;

   console.log(app_path() + '/video/' + videoid + '.mp4');
   
   fs.exists(app_path() + '/video/'+videoid+'.mp4', function (exists) {
      if(exists)
         res.sendfile(app_path() + '/video/'+videoid +'.mp4');
      else
         res.send({"ok": false, "error":"Video ID does not exist"});
   });
   
}

