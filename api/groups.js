var dbase = require('./dbase');
var uuid = require('./uuid');
var apns_lib = require('./apns_lib');

db = dbase.db;

// VideoGroup APIs
//app.get('/group/view/:groupid', groups.view);
//app.post('/group/create', groups.create);
//app.get('/group/list/:userid', groups.list);


// GET /group/view/:groupid

exports.view = function(req, res) {
   
   var groupid = req.params.groupid;
   
   db.collection('groups', function(err, collection) {
      if(err) {
         res.send({'ok': false, 'error': 'Database error'});
         return;
      }
      
      var result = [];
      var result2 = [];
      collection.findOne({'_id':groupid}, function(err, item) {
         if(!item) {
            res.send({ok:false,error:'No matching groupid'});
            return;
         }
         var i = {};
         var final_data = {ok:true, members:{}}
         final_data.desc = item.desc;
         final_data.tags = item.tags;
         final_data.title = item.title;
         final_data.msg = item.msg;
         i.isCreator = true;
         i.userid = item.creator;
         if(item.videoid)
            i.videoid = item.videoid;
         else
            i.videoid = null;
         result.push(i);
         
         for (member in item.members) {
            the_member = item.members[member];
            the_member.isCreator = false;
            result.push(the_member);
         }
         
         db.collection('users', function(err, collection_user) {
         
            result.forEach(function(item) {
               collection_user.findOne({'_id': item.userid}, function(err, u) {
                  if(u) {
                     item.exist = true;
                     result2.push(item);
                  } else {
                     item.exist = false;
                     result2.push(item);
                  }
                  if(result.length == result2.length) {
                     final_data.members = result2;
                     res.send(final_data);
                     //res.send({'ok':true, 'members':result2});
                  }
               });
            });
         });
         
      });
   });
}

// delete /group/delete/:groupid/:userid
exports.delete = function(req, res) {

   var groupid = req.params.groupid;
   var userid = req.params.userid;

   db.collection('groups', function(err, collection) {
      if(err) {
         res.send({'ok': false, 'error': 'Database error'});
         return;
      }
         
      collection.update({'_id':groupid, 'creator':userid}, {$set:{'deleted':true}}, function(err, items) {
         if(err) {
            res.send({"ok":false, "error": err});
         }
         if(!items) {
            collection.update({'_id':groupid, 'members.userid':userid}, {$set:{'members.$.deleted':true}}, function(err, items) {
               if(err) {
                  res.send({"ok":false, "error": err});
               }
               if(!items) {
                  res.send({"ok":false, "error": "User does not belong to this group or user does not exist."});
               } else {
                  res.send({"ok":true});
               }
            });
         } else {
            res.send({"ok":true});
         }
      });
   });
}


// GET /group/list/:userid
exports.list = function(req, res) {

   var userid = req.params.userid;
   var result = { 'ok': false, 'groups': []};

   db.collection('groups', function(err, collection) {
      if(err) {
         res.send({'ok': false, 'error': 'Database error'});
         return;
      }
      
      collection.find({$or : [{'creator': userid} ,{'members.userid':userid}]}, {sort: {"time":-1}}).toArray(function(err, items) {
      item_loop:
         for (item in items) {
            the_item = items[item];
            var group_item = {};
            group_item.groupid = the_item['_id'];
            if(!the_item.final_videoid)
               group_item.final_videoid = null;
            else
               group_item.final_videoid = the_item.final_videoid;

            if (the_item.creator == userid) {
               if(the_item.deleted) continue;
               // count video uploaded
               var uploaded = 0;
               for(m in the_item.members) {
                  if(the_item.members[m].videoid)
                     uploaded++;
               }

               group_item.isCreator = true;
               group_item.creator = userid;
               //group_item.title = "Group created by you [" + userid + "]";
               group_item.title = the_item.title;
               group_item.subtitle = "Video received: [" + uploaded + "/" + the_item.members.length + "]";
               if(!the_item.videoid)
                  group_item.videoid = null;
               else
                  group_item.videoid = the_item.videoid;
            } else {
               group_item.isCreator = false;
               group_item.creator = the_item.creator;
               group_item.title = the_item.creator_name;
               group_item.subtitle = "Waiting for upload";
               for (i in the_item.members) {
                  if (the_item.members[i].userid == userid) {
                     if(the_item.members[i].deleted) continue item_loop;
                     group_item.videoid = the_item.members[i].videoid;
                     if(group_item.videoid)
                        group_item.subtitle = "Video uploaded";
                     break;
                  }
               }
            }
            if(the_item.final_videoid)
               group_item.subtitle = "Final video is created.";
            group_item.time = the_item.time;
            
            result['groups'].push(group_item);
         }
         result['ok'] = true;
         res.send(result);
         return;
      });
   });
}

// POST /group/create
exports.create = function(req, res) {
   var body = req.body;
   
   var groupid = uuid.uuid(16);
   
   var data = {_id: groupid, desc:body.desc, tags:body.tags, title:body.title, msg:body.msg, creator: body.creator, time:new Date(), members:[]};

   db.collection('users', function(err, collection) {

      var members = body.members;
      var members_name = [];
      var creator_name = '';
   
      start();
      
      function start() {
         collection.findOne({_id:body.creator}, function(err, item) {
            if(item) {
               creator_name = item.username;
               data.creator_name = creator_name;
               find_members_name();
            } else {
               res.send({'ok':false,'error':'No such creator'});
            }
         });
      }
   
      function find_members_name() {
         members.forEach(function(member) {
            collection.findOne({_id:member}, function(err, item) {
               if(item) {
                  // This member already in our DB
                  members_name.push({userid:member,username:item.username,videoid:null});
               } else {
                  // This member not in our DB
                  members_name.push({userid:member,username:null,videoid:null});
               }
               if(members_name.length == members.length) {
                  data.members = members_name;
                  final();
               }
            });
         });
      }      
   
      /*
      for (member in body.members) {
         the_member = body.members[member];
         data.members.push({"userid":the_member, "videoid":null});
      }
      */
   
      function final() {
         collection.findOne({'_id':body.creator}, function(err, item) {
            if (err) {
               res.send({'ok': false, 'error': "Database error"});
            } else {
               if(!item) {
                  res.send({'ok': false, 'error':'User '+body.creator+' not found'});
                  return;
               } else {
                  db.collection('groups', function(err, collection) {
                     collection.insert(data, {safe:true}, function(err, result) {
                        if (err) {
                           res.send({'ok': false, 'error': "Group creation failed"});
                        } else {
                           for(x in data.members) {
                              apns_lib.sendMessage(data.members[x].userid, creator_name +' invited you to make a group video.');
                           }
                           res.send({'ok': true, 'groupid':groupid});
                        }
                     });
                  });
               }
            }
         });
      }
   });
}

