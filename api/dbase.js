var mongo = require('mongodb');

var Server = mongo.Server,
    Db = mongo.Db,
    BSON = mongo.BSONPure;
    
var server = new Server('localhost', 27017, {auto_reconnect: true});

console.log('Initializing DB');

db = new Db('gv_server', server, {w:1});

db.open(function(err, db) {
   if(!err) {
      console.log("Connected to 'gv_server' database");
   } else {
      console.log("Failed to open db");
   }
});

module.exports.db = db;