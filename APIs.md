# Group Video Server APIs

## User
==============================================
### Register user

`POST /user`

Body:

	{
		"userid": "david",
		"password": "12345"
	}
	
Successful response:

	{
		"ok": true
	}

Error response:

	{
		"ok": false,
		"error": "UserID already exists"
	}
	
Test command:

	curl -i -X POST -H 'Content-Type: application/json' -d '{"userid":"david","password":"12345"}' http://gvserver.no-ip.org:8080/user


### Login

`POST /user/login`

Body:

Include the push notification token in the login.

	{
		"userid": "david",
		"password": "12345",
		"token": "xxxxxxxxxxxxxxxxxxx"
	}
	
Successful response:

	{
		"ok": true
	}

Error response:

	{
		"ok": false,
		"error": "Login failed"
	}

Test command:

	curl -i -X POST -H 'Content-Type: application/json' -d '{"userid":"david","password":"12345","token":"asdasdasdasdasd123123"}' http://gvserver.no-ip.org:8080/user/login


### UserInfo

`GET /user/:userid`

Body:

	None
	
Successful response:

	{
		"ok": true
		"user_info": { The user information }
	}
	
Error response:

	{
		"ok": false
		"error": "User does not exist"
	}
	
Test command:

	curl -i -X GET http://gvserver.no-ip.org:8080/user/david


### Update Token

`POST /update_token`

Body:

Include the push notification token in the login.

	{
		"userid": "david",
		"token": "xxxxxxxxxxxxxxxxxxx"
	}
	
Successful response:

	{
		"ok": true
	}

Error response:

	{
		"ok": false,
		"error": "Update failed"
	}

Test command:

	curl -i -X POST -H 'Content-Type: application/json' -d '{"userid":"david","token":"asdasdasdasdasd123123"}' http://gvserver.no-ip.org:8080/update_token


### Delete User

`DELETE /user/:userid`

Body:

	None
	
Successful response:

	{
		"ok": true
	}
	
Error response:

	{
		"ok": false
		"error": "User not exists"
	}
	
Test command:

	curl -i -X DELETE http://gvserver.no-ip.org:8080/user/david
	

### Send a test notification

`GET /pushtest/:userid`

Body:

	None
	
Successful response:

	{
		"ok": true
	}
	
Error response:

	{
		"ok": false
		"error": "User does not exist"
	}
	
Test command:

	curl -i -X GET http://gvserver.no-ip.org:8080/pushtest/david

	

## Group
==============================================
### Create group

`POST /group/create`

Body:

	{
		"creator": "david",
		"members": ["peter", "eric","paul"]
	}
	
Successful response:

	{
		"ok": true
		"groupid": "xxxxxxxx"
	}

Error response:

	{
		"ok": false,
		"error": "Error Message"
	}
	
Test command:

	curl -i -X POST -H 'Content-Type: application/json' -d '{"creator":"david","members":["peter","eric","paul"]}' http://gvserver.no-ip.org:8080/group/create

### Delete group

`DELETE /group/delete/:groupid/:userid`

Body:

	None
	
Successful response:

	{
		"ok": true
	}

Error response:

	{
		"ok": false,
		"error": "Error Message"
	}
	
Test command:

	curl -i -X DELETE http://gvserver.no-ip.org:8080/group/delete/XXXXXXXX/uuuuuuuu


### List user's groups

`GET /group/list/:userid`

Body:

	None
	
Successful response:

	{
		"ok": true,
		"groups": [ 
					{"groupid" : "xxxxxxxx",
					 "final_videoid": null,
					 "creator: "xxxxxxx"
					 "isCreator" : true,
					 "time" : "2013-08-13T06:21:24",
					 "title" : "vincent",
					 "videoid": null,
					 "subtitle" : "Video received (1/4)"},
					
					{"groupid" : "xxxxxxx",
					 "final_videoid": null,
					 "creator: "xxxxxxx"
					 "isCreator" : false,
					 "time" : "2013-08-13T07:21:24",
					 "title" : "Video request from: [eddie]",
					 "videoid": null,
					 "subtitle" : "Waiting for your video"},
					 
					{"groupid" : "xxxxxxx",
					 "final_videoid": null,
					 "creator: "xxxxxxx"
					 "isCreator" : false,
					 "time" : "2013-08-13T08:21:24",
					 "title" : "Video request from: [eddie]",
					 "videoid": null,
					 "subtitle" : "Waiting for your video"}
				  ]
	}

Error response:

	{
		"ok": false,
		"error": "Error Message"
	}
	
Test command:

	curl -i -X GET http://gvserver.no-ip.org:8080/group/list/david

### View a groups

`GET /group/view/:groupid`

Body:

	None
	
Successful response:

	{
		"ok": true,
		"members": [ 
					{"userid" : "david",
					 "exist" : true, 
					 "videoID" : "xxxxxxx"}, 

					{"userid" : "eddie",
					 "exist" : true, 
					 "videoID" : "xxxxxxx"}, 

					{"userid" : "david",
					 "exist" : true, 
					 "videoID" : "xxxxxxx"}
				    ] 
	}

Error response:

	{
		"ok": false,
		"error": "Error Message"
	}
	
Test command:

	curl -i -X GET http://gvserver.no-ip.org:8080/group/view/xxxxxxxxx


## Video APIs
==============================================
### Upload Video

`POST /video/upload/:groupid/:userid`

Test command:

	curl -F "video=@11m.mp4" http://localhost:8080/video/upload/ohqjzQxKpOY3Q4nm/charlie

### Upload Thumbnail

`POST /video/thumb_upload/:videoid`

Test command:

	curl -F "thumbnail=@1234.png" http://localhost:8080/video/thumb_upload/ohqjzQxKpOY3Q4nm
	
### Upload Final Video

`POST /video/fupload/:groupid`

Test command:

	curl -F "video=@11m.mp4" http://localhost:8080/video/fupload/ohqjzQxKpOY3Q4nm

### Download Video

`POST /video/:videoid`

Test command:

	curl -i -X GET http://gvserver.no-ip.org:8080/video/xxxxxxxxx


### Download Thumbnail

`POST /video/thumbnail/:videoid`

Test command:

	curl -i -X GET http://gvserver.no-ip.org:8080/video/thumbnail/xxxxxxxxxx
